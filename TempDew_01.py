#importing modules
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestRegressor
#from sklearn.svm import SVR
from sklearn.preprocessing import Imputer, LabelEncoder,OneHotEncoder
from math import sqrt
import numpy as np
import matplotlib.pyplot as plt


#importing dataset
df = pd.read_csv(r'C:\Oveis\Programming\Projects\Ali_Rashid\Full_dataset.csv', index_col=['Date'])
df.index=pd.to_datetime(df.index)
df.drop(['Year','Month','Day'],axis=1, inplace=True)
Col_Names = ['Station_Name', 'No_Day','Max_Temp','Min_Temp','Total_Solar_Rad','Avg_Wind_Speed','RH','ET_Grass']
df.columns = Col_Names
df['DOY'] = df.index.dayofyear
df.head()

#removing years prior to 2003
df=df['2003':'2016']
df=df.sort_index()

#extract X and y
X=df[['Min_Temp','Max_Temp','Avg_Wind_Speed']]
y=df['ET_Grass']


#implementing Imputer while using Avg_Wind_Speed
if 'Avg_Wind_Speed' in list(X.columns):
    imputer = Imputer()
    X.loc[:,'Avg_Wind_Speed']=imputer.fit_transform(X.loc[:,'Avg_Wind_Speed'].reshape(-1,1))

#Split the data to train and test sets
R_square_linear=dict()
RMSE_linear=dict()
R_square_RF=dict()
RMSE_RF=dict()
#Year base train test split
for i in range (2003,2017):
    X_test=X[str(i)]
    y_test=y[str(i)]
    X_train=X[df.index.year!=i]
    y_train=y[df.index.year!=i]
#fitting linear model
    linReg = LinearRegression()
    linReg.fit(X_train, y_train)
    y_pred = linReg.predict(X_test)
    R_square_linear[i]=linReg.score(X_test, y_test)
    RMSE_linear[i]=sqrt(mean_squared_error(y_test, y_pred))
    #print('R Square of Linear model is: {}'.format(linReg.score(X_test, y_test)))
    #print('RMSE of Linear model is: {}'.format(sqrt(mean_squared_error(y_test, y_pred))))
    #df.iloc[:,2:5].hist()

#fitting random forest model
    RFreg = RandomForestRegressor(n_estimators=100, random_state=13, min_samples_leaf=30)
    RFreg.fit(X_train, y_train)
    y_pred=RFreg.predict(X_test)
    R_square_RF[i]=RFreg.score(X_test, y_test)
    RMSE_RF[i]=sqrt(mean_squared_error(y_test, y_pred))
    #print('R Square of Random Forest model is: {}'.format(RFreg.score(X_test, y_test)))
    #print('RMSE of Random Forest model is: {}'.format(sqrt(mean_squared_error(y_test, y_pred))))
    


#Station base train test split
i='Fargo'
R_square_linear_StationBased=dict()
RMSE_linear_StationBased=dict()
R_square_RF_StationBased=dict()
RMSE_RF_StationBased=dict()
for i in list(df.Station_Name.unique()):
    X_test=X[df.Station_Name==i]
    y_test=y[df.Station_Name==i]
    X_train=X[df.Station_Name!=i]
    y_train=y[df.Station_Name!=i]
#fitting linear model
    linReg = LinearRegression()
    linReg.fit(X_train, y_train)
    y_pred = linReg.predict(X_test)
    R_square_linear_StationBased[i]=linReg.score(X_test, y_test)
    RMSE_linear_StationBased[i]=sqrt(mean_squared_error(y_test, y_pred))

#fitting random forest model
    RFreg = RandomForestRegressor(n_estimators=100, random_state=13, min_samples_leaf=30)
    RFreg.fit(X_train, y_train)
    y_pred=RFreg.predict(X_test)
    R_square_RF_StationBased[i]=RFreg.score(X_test, y_test)
    RMSE_RF_StationBased[i]=sqrt(mean_squared_error(y_test, y_pred))
R_square_RF_StationBased[i]
    
# =============================================================================
# #fitting SVR model
# SVRreg = SVR()
# SVRreg.fit(X_train, y_train)
# y_pred = SVRreg.predict(X_test)
# print('R Square of SVR model is: {}'.format(SVRreg.score(X_test, y_test)))
# print('MSE of SVR model is: {}'.format(mean_squared_error(y_test, y_pred)))
# =============================================================================
print('*'*80)
##fitting AdaBoostRegressor
#Gradreg = GradientBoostingRegressor(n_estimators=1000, random_state=13, learning_rate=0.02, min_samples_split=50)
#Gradreg.fit(X_train, y_train)
#y_pred=Gradreg.predict(X_test)
#print('R Square of Ada Boost model is: {}'.format(Gradreg.score(X_test, y_test)))
#print('RMSE of Ada Boost model is: {}'.format(sqrt(mean_squared_error(y_test, y_pred))))


# =============================================================================
# 
# #Taking care of categorical variable: Station_Name
# 
# X=df[['Station_Name','Min_Temp','Max_Temp', 'DOY', 'Total_Solar_Rad']]
# y=df['ET_Grass']
# 
# label = LabelEncoder()
# X.iloc[:,0] = label.fit_transform(X.Station_Name)
# oneHotEncoder = OneHotEncoder(categorical_features=[0])
# X = oneHotEncoder.fit_transform(X).toarray()
# X = X[:,1:]
# X=pd.DataFrame(X)
# X.index=y.index
# X_train = X[:'2012']
# X_test = X['2013':]
# y_train = y[:'2012']
# y_test = y['2013':]
# 
# RFreg = RandomForestRegressor(n_estimators=200, random_state=13, min_samples_leaf=30)
# RFreg.fit(X_train, y_train)
# y_pred=RFreg.predict(X_test)
# 
# print('R Square of Random Forest model is: {}'.format(RFreg.score(X_test, y_test)))
# print('RMSE of Random Forest model is: {}'.format(sqrt(mean_squared_error(y_test, y_pred))))
# print('RMSE of Random Forest model for train set is: {}'.format(sqrt(mean_squared_error(y_train, RFreg.predict(X_train)))))
# print('R Square of Random Forest model for train set is: {}'.format(RFreg.score(X_train, y_train)))
# =============================================================================
# =============================================================================
# 
# l=[]
# for i in range(13):
#     count=y[np.logical_and(y>=i, y<i+1)]
#     count=count.shape[0]
#     l.append(count)
#     
# plt.bar(range(13),l)
# plt.xlabel('Evapotranspiration mm/day'), plt.ylabel('Frequency')
# plt.savefig(r'C:\Users\Oveis\Desktop\Test.jpg', dpi=600)
# =============================================================================
