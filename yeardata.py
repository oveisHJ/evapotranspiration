#Temporal
def temporal(df, X, y):
    R_square_linear_station=dict()
    RMSE_linear_station=dict()
    R_square_RF_station=dict()
    RMSE_RF_station=dict()
    for j in list(df.Station_Name.unique()):
        R_square_linear=dict()
        RMSE_linear=dict()
        R_square_RF=dict()
        RMSE_RF=dict()
        station=X[df.Station_Name==j]
        y_station=y[df.Station_Name==j]
        for i in range (2003,2017):
            X_test=station[str(i)]
            y_test=y_station[str(i)]
            X_train=station[station.index.year!=i]
            y_train=y_station[station.index.year!=i]
        #fitting linear model
            linReg = LinearRegression()
            linReg.fit(X_train, y_train)
            y_pred = linReg.predict(X_test)
            R_square_linear[i]=linReg.score(X_test, y_test)
            RMSE_linear[i]=sqrt(mean_squared_error(y_test, y_pred))
        #fitting random forest model
            RFreg = RandomForestRegressor(n_estimators=100, random_state=13, min_samples_leaf=30)
            RFreg.fit(X_train, y_train)
            y_pred=RFreg.predict(X_test)
            R_square_RF[i]=RFreg.score(X_test, y_test)
            RMSE_RF[i]=sqrt(mean_squared_error(y_test, y_pred))
        RMSE_RF_station[j]=RMSE_RF
        RMSE_linear_station[j]=RMSE_linear
        R_square_RF_station[j]=R_square_RF
        R_square_linear_station[j]=R_square_linear
    return (R_square_linear_station, R_square_RF_station, RMSE_linear_station, RMSE_RF_station)