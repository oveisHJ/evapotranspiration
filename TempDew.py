#importing modules
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestRegressor
#from sklearn.svm import SVR
from sklearn.preprocessing import Imputer
from math import sqrt
#import numpy as np
#import matplotlib.pyplot as plt
#import yeardata

#importing dataset
df = pd.read_csv(r'C:\Oveis\Programming\Projects\Ali_Rashid\Full_dataset.csv', index_col=['Date'])
df.index=pd.to_datetime(df.index)
df.drop(['Year','Month','Day'],axis=1, inplace=True)
Col_Names = ['Station_Name', 'No_Day','Max_Temp','Min_Temp','Total_Solar_Rad','Avg_Wind_Speed','RH','ET_Grass']
df.columns = Col_Names
df['DOY'] = df.index.dayofyear
df.head()

#removing years prior to 2003
df=df['2003':'2016']
df=df.sort_index()

#extract X and y
X=df[['Min_Temp','Max_Temp']]
y=df['ET_Grass']


#implementing Imputer while using Avg_Wind_Speed
if 'Avg_Wind_Speed' in list(X.columns):
    imputer = Imputer()
    X.loc[:,'Avg_Wind_Speed']=imputer.fit_transform(X.loc[:,'Avg_Wind_Speed'].reshape(-1,1))

#Split the data to train and test sets


#Station base train test split
def Station_Based(n_estimators=100, min_samples_leaf=40):
    R_square_linear_StationBased=dict()
    RMSE_linear_StationBased=dict()
    R_square_RF_StationBased=dict()
    RMSE_RF_StationBased=dict()
    resid=dict()
    for i in list(df.Station_Name.unique()):
        X_test=X[df.Station_Name==i]
        y_test=y[df.Station_Name==i]
        X_train=X[df.Station_Name!=i]
        y_train=y[df.Station_Name!=i]
    #fitting linear model
        linReg = LinearRegression()
        linReg.fit(X_train, y_train)
        y_pred = linReg.predict(X_test)
        R_square_linear_StationBased[i]=linReg.score(X_test, y_test)
        RMSE_linear_StationBased[i]=sqrt(mean_squared_error(y_test, y_pred))
        resid[i]= y_pred - y_test
    #fitting random forest model
        RFreg = RandomForestRegressor(n_estimators=n_estimators, random_state=13, min_samples_leaf=min_samples_leaf)
        RFreg.fit(X_train, y_train)
        y_pred=RFreg.predict(X_test)
        R_square_RF_StationBased[i]=RFreg.score(X_test, y_test)
        RMSE_RF_StationBased[i]=sqrt(mean_squared_error(y_test, y_pred))
        #resid[i]= y_pred - y_test
        
    #return (R_square_RF_StationBased, R_square_linear_StationBased, RMSE_RF_StationBased, RMSE_linear_StationBased)
    return resid
'''**********************************************************************************'''
'''**********************************************************************************'''
def Station_Year_Based(n_estimators=100, min_samples_leaf=40):
    R_square_linear_station=dict()
    RMSE_linear_station=dict()
    R_square_RF_station=dict()
    RMSE_RF_station=dict()
    Linear_resid_station=dict()
    RF_resid_station=dict()
    count=dict()
    SI=dict()
    for j in list(df.Station_Name.unique()):
        R_square_linear=dict()
        RMSE_linear=dict()
        R_square_RF=dict()
        RMSE_RF=dict()
        linear_resid=dict()
        rf_resid=dict()
        count_year=dict()
        SI_year=dict()
        station=X[df.Station_Name==j]
        y_station=y[df.Station_Name==j]
        
        for i in range (2003,2017):
            X_test=station[str(i)]
            y_test=y_station[str(i)]
            X_train=station[station.index.year!=i]
            y_train=y_station[station.index.year!=i]
        #fitting linear model
            linReg = LinearRegression()
            linReg.fit(X_train, y_train)
            y_pred = linReg.predict(X_test)
            R_square_linear[i]=linReg.score(X_test, y_test)
            RMSE_linear[i]=sqrt(mean_squared_error(y_test, y_pred))
            linear_resid[i]= abs(y_pred - y_test).mean()
        #fitting random forest model
            RFreg = RandomForestRegressor(n_estimators=n_estimators, random_state=13, min_samples_leaf=min_samples_leaf)
            RFreg.fit(X_train, y_train)
            y_pred=RFreg.predict(X_test)
            R_square_RF[i]=RFreg.score(X_test, y_test)
            RMSE_RF[i]=sqrt(mean_squared_error(y_test, y_pred))
            rf_resid[i]= abs(y_pred - y_test).mean()
            count_year[i]=y_test.count()
            SI_year[i]=RMSE_RF[i]/count_year[i]
        RMSE_RF_station[j]=RMSE_RF
        RMSE_linear_station[j]=RMSE_linear
        R_square_RF_station[j]=R_square_RF
        R_square_linear_station[j]=R_square_linear
        RF_resid_station[j]=rf_resid
        Linear_resid_station[j]=linear_resid
        count[j]=count_year
        SI[j]=SI_year
    #return (R_square_RF_station, R_square_linear_station, RMSE_RF_station, RMSE_linear_station)
    return (count, SI,RMSE_RF)
count, SI_index,RMSE_RF= Station_Year_Based(50,35)

#rf,linear, rmse_rf, rmse_l= Station_Based(50,35)
#RF=pd.DataFrame(RF)
#Linear=pd.DataFrame(Linear)
#RF_MAE=pd.DataFrame(MAE,(1,))
#RF.to_csv(r'C:\Oveis\Programming\Projects\Ali_Rashid\Results\RF_MAE_Station_Year_Based_MM.csv')
#Linear.to_csv(r'C:\Oveis\Programming\Projects\Ali_Rashid\Results\Linear_MAE_Station_Year_Based_MM.csv')
# =============================================================================
# R2Linear=pd.DataFrame(linear,(1,))
# R2Linear.to_csv(r'C:\Oveis\Programming\Projects\Ali_Rashid\Results\Multiple_Linear\Linear_Station_Based_MM.csv')
# 
# RMSELinear=pd.DataFrame(rmse_l,(1,))
# RMSELinear.to_csv(r'C:\Oveis\Programming\Projects\Ali_Rashid\Results\Multiple_Linear\RMSE_Linear_Station_Based_MM.csv')
# =============================================================================

# =============================================================================
# import matplotlib.pyplot as plt
# Fargo=rf['Fargo'].values()
# years=rf['Fargo'].keys()
# 
# plt.scatter(years, Fargo)
# ylimit=plt.ylim(0,1)
# plt.show()
# =============================================================================

#one, two, three, four=yeardata.temporal(df, X, y)
# =============================================================================
# ============================================================================
    
# l=[]
# for i in range(13):
#     count=y[np.logical_and(y>=i, y<i+1)]
#     count=count.shape[0]
#     l.append(count)
#     
# plt.bar(range(13),l)
# plt.xlabel('Evapotranspiration mm/day'), plt.ylabel('Frequency')
# plt.savefig(r'C:\Users\Oveis\Desktop\Test.jpg', dpi=600)
# =============================================================================


# =============================================================================
# def Year_Based(n_estimators=100, min_samples_leaf=40):
#     R_square_linear=dict()
#     RMSE_linear=dict()
#     R_square_RF=dict()
#     RMSE_RF=dict()
#     #Year base train test split
#     for i in range (2003,2017):
#         X_test=X[str(i)]
#         y_test=y[str(i)]
#         X_train=X[df.index.year!=i]
#         y_train=y[df.index.year!=i]
#     #fitting linear model
#         linReg = LinearRegression()
#         linReg.fit(X_train, y_train)
#         y_pred = linReg.predict(X_test)
#         R_square_linear[i]=linReg.score(X_test, y_test)
#         RMSE_linear[i]=sqrt(mean_squared_error(y_test, y_pred))
#         #print('R Square of Linear model is: {}'.format(linReg.score(X_test, y_test)))
#         #print('RMSE of Linear model is: {}'.format(sqrt(mean_squared_error(y_test, y_pred))))
#         #df.iloc[:,2:5].hist()
#     
#     #fitting random forest model
#         RFreg = RandomForestRegressor(n_estimators=n_estimators, random_state=13, min_samples_leaf=min_samples_leaf)
#         RFreg.fit(X_train, y_train)
#         y_pred=RFreg.predict(X_test)
#         R_square_RF[i]=RFreg.score(X_test, y_test)
#         RMSE_RF[i]=sqrt(mean_squared_error(y_test, y_pred))
#         #print('R Square of Random Forest model is: {}'.format(RFreg.score(X_test, y_test)))
#         #print('RMSE of Random Forest model is: {}'.format(sqrt(mean_squared_error(y_test, y_pred))))
#     return (R_square_RF, R_square_linear, RMSE_RF, RMSE_linear)
# 
# =============================================================================
