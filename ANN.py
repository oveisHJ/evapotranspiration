from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.optimizers import SGD
import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
import h5py

df = pd.read_csv(r'C:\Oveis\Programming\Projects\Ali_Rashid\Full_dataset.csv', index_col=['Date'])
df.index = pd.to_datetime(df.index)
df.drop(['Year', 'Month', 'Day'], axis=1, inplace=True)
Col_Names = ['Station_Name', 'No_Day', 'Max_Temp', 'Min_Temp', 'Total_Solar_Rad', 'Avg_Wind_Speed', 'RH', 'ET_Grass']
df.columns = Col_Names
df['DOY'] = df.index.dayofyear
df.head()


#create train and test sets
X = df[['Min_Temp', 'Max_Temp', 'DOY', 'Total_Solar_Rad']]
y = df['ET_Grass']

#fitting scaler to the X dataset
sc = MinMaxScaler()
X_scaled = sc.fit_transform(X)
X_scaled = pd.DataFrame(X_scaled, index=y.index)

#Splitting data to train and test set
X_train = X_scaled[:'2002']
X_test = X_scaled['2003':]
y_train = y[:'2002']
y_test = y['2003':]



n_cols = X.shape[1]
input_shape = (n_cols,)
node = 20
activation = 'relu'
def get_new_model(input_shape=input_shape):
    model = Sequential()
    model.add(Dense(node, activation=activation, input_shape=input_shape))
    model.add(Dense(node, activation=activation))
    model.add(Dense(node, activation=activation))
    model.add(Dense(node, activation=activation))
    model.add(Dense(node, activation=activation))




    model.add(Dense(1))
    return model

model = get_new_model()
my_optimizer = 'adam'
# sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(optimizer=my_optimizer, loss='mean_squared_error')
# model.compile(optimizer=sgd, loss='mean_squared_error')
H = model.fit(X_train, y_train, batch_size=1000, epochs=100)
print(np.min(H.history['loss']),'RMSE is: {}'.format(np.sqrt(np.min(H.history['loss']))))

y_pred = model.predict(X_test)
print(mean_squared_error(y_test, y_pred),'RMSE is: {}'.format(np.sqrt(mean_squared_error(y_test, y_pred))))


model.save('rnn.h5')