from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout
import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
import h5py

df = pd.read_csv(r'C:\Oveis\Programming\Projects\Ali_Rashid\Full_dataset.csv', index_col=['Date'])
df.index = pd.to_datetime(df.index)
df.drop(['Year', 'Month', 'Day'], axis=1, inplace=True)
Col_Names = ['Station_Name', 'No_Day', 'Max_Temp', 'Min_Temp', 'Total_Solar_Rad', 'Avg_Wind_Speed', 'RH', 'ET_Grass']
df.columns = Col_Names
df['DOY'] = df.index.dayofyear
df.head()


#create train and test sets
X = df[['Min_Temp', 'Max_Temp', 'DOY', 'Total_Solar_Rad']]
Fargo_2012 = np.logical_and(df.No_Day<=41274,df.Station_Name == 'Fargo')
X = df.loc[Fargo_2012, ['Min_Temp', 'Max_Temp', 'DOY', 'Total_Solar_Rad']]
y = df.loc[Fargo_2012,'ET_Grass']

#fitting scaler to the X dataset
sc = MinMaxScaler()
X_scaled = sc.fit_transform(X)
#X_scaled = pd.DataFrame(X_scaled, index=y.index)


def LSTM_X_Train(X_scaled):


    y_train = []
    minTemp = []
    for i in range(60, 4749):
        minTemp.append(X_scaled[i-60:i,0])
        y_train.append(y[i])
    minTemp, y_train = np.array(minTemp), np.array(y_train)
    minTemp = np.reshape(minTemp, (4689, 60, 1))

    maxTemp = []
    for i in range(60, 4749):
        maxTemp.append(X_scaled[i-60:i, 1])

    maxTemp = np.array(maxTemp)
    maxTemp = np.reshape(maxTemp, (4689, 60, 1))

    DofY = []
    for i in range(60, 4749):
        DofY.append(X_scaled[i-60:i, 2])

    DofY = np.array(DofY)
    DofY = np.reshape(DofY, (4689, 60, 1))

    Solar = []
    for i in range(60, 4749):
        Solar.append(X_scaled[i-60:i, 3])

    Solar = np.array(Solar)
    Solar = np.reshape(Solar, (4689, 60, 1))

    x_dataset = np.concatenate((DofY, minTemp, maxTemp, Solar), axis=2)
    return (x_dataset, y_train)


X_train, y_train = LSTM_X_Train(X_scaled)

print('it is done')

reg = Sequential()
reg.add(LSTM(units=50, return_sequences=True, input_shape=(60, 4)))
reg.add(Dropout(0.2))
reg.add(LSTM(units=50, return_sequences=True))
reg.add(Dropout(0.2))
reg.add(LSTM(units=50, return_sequences=True))
reg.add(Dropout(0.2))
reg.add(LSTM(units=50))
reg.add(Dropout(0.2))
reg.add(Dense(units=1))
reg.compile(optimizer='adam', loss='mean_squared_error')
H = reg.fit(X_train, y_train, epochs=5, batch_size=8)

reg.save('rnn.h5')
# H = model.fit(X_train, y_train, batch_size=1000, epochs=400)
print(np.min(H.history['loss']),'RMSE is: {}'.format(np.sqrt(np.min(H.history['loss']))))
#
# y_pred = model.predict(X_test)
# print(mean_squared_error(y_test, y_pred), 'RMSE is: {}'.format(np.sqrt(mean_squared_error(y_test, y_pred))))
#
#
